﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;

namespace SplitScreen
{
    public partial class frmPincipale : Form
    {
        [DllImport("user32.dll",SetLastError = true, CharSet = CharSet.Auto)]
        public static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern bool MoveWindow(IntPtr hWnd, int x, int y, int nWitdth, int nHeight, bool bRepaint);

        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, SetWindowPosFlags uFlags);

        [Flags()]
        private enum SetWindowPosFlags : uint
        {
            SynchronousWindowPosition = 0x4000,
            DeferErase = 0x2000,
            DrawFrame = 0x0020,
            FrameChanged = 0x0020,
            HideWindow = 0x0080,
            DoNotActivate = 0x0010,
            DoNotCopyBits = 0x0100,
            IgnoreMove = 0x0002,
            DoNotChangeOwnerZOrder = 0x0200,
            DoNotRedraw = 0x0008,
            DoNotReposition = 0x0200,
            DoNotSendChangingEvent = 0x0400,
            IgnoreResize = 0x0001,
            IgnoreZOrder = 0x0004,
            ShowWindow = 0x0040,
        }

        ProcessStartInfo infoTera1;
        ProcessStartInfo infoTera2;
        ProcessStartInfo infoChrome;
        Process pTera1 = new Process();
        Process pTera2 = new Process();
        Process pChrome = new Process();
        System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
        INI.IniFile setup = new INI.IniFile(Application.StartupPath + "\\setup.ini");
        Rectangle workingArea = Screen.PrimaryScreen.WorkingArea;
        int larghezza, altezza, wTera, hTera;
        NotifyIcon myNotifyIcon;

        public frmPincipale()
        {
            InitializeComponent();

        }
        
        void Temporizzatore(object sender, EventArgs e)
        {
            SpostaFinestre();
        }
        void SpostaFinestre()
        {
            while (pTera1.MainWindowHandle.Equals(IntPtr.Zero))
            {
                Thread.Sleep(50);
                pTera2.Refresh();
            }
            MoveWindow(pTera1.MainWindowHandle, 1, 1, wTera, hTera, true);

            while (pTera2.MainWindowHandle.Equals(IntPtr.Zero))
            {
                Thread.Sleep(50);
                pTera2.Refresh();
            }
            MoveWindow(pTera2.MainWindowHandle, 1, hTera, wTera, hTera, true);

            Process[] processes = Process.GetProcessesByName("chrome");
            foreach (Process p in processes)
            {
                IntPtr windowHandle = p.MainWindowHandle;
                SetWindowPos(windowHandle, IntPtr.Zero, wTera - 20, 1, larghezza - wTera + 20, hTera * 2 + 5, 0);
            }
        }

        void AvvioProgrammi()
        {
            pChrome.Start();
            pTera1.Start();
            pTera2.Start();
        }
        void Exit(object sender, EventArgs e)
        {
            // Hide tray icon, otherwise it will remain shown until user mouses over it
            myNotifyIcon.Visible = false;

            TerminaProcessi();
            Application.Exit();
        }
        void TerminaProcessi()
        {
            try
            {
                pTera1.Kill();
            }
            catch { }

            try
            {
                pTera2.Kill();
            }
            catch { }

            Process[] processes = Process.GetProcessesByName("chrome");
            foreach (Process p in processes)
            {
                IntPtr windowHandle = p.MainWindowHandle;
                Console.WriteLine("title:" + p.StartTime);
                try
                {
                    p.Kill();
                }
                catch { }
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            myNotifyIcon = new NotifyIcon()
            {
                Icon = Properties.Resources.Icona,
                ContextMenu = new ContextMenu(new MenuItem[] {
                new MenuItem("Exit", Exit)
            }),
                Visible = true
            };
            this.Visible = false;
            myTimer.Tick += new EventHandler(Temporizzatore);
            myTimer.Interval = 5000;
            larghezza = workingArea.Width;
            altezza = workingArea.Height;
            wTera = 660;
            hTera = 360;
            infoTera1 = new ProcessStartInfo(setup.GetValue("config", "teratermlocation"));
            infoTera2 = new ProcessStartInfo(setup.GetValue("config", "teratermlocation"));
            infoChrome = new ProcessStartInfo(setup.GetValue("config","urlInfor"));

            pTera1.StartInfo = infoTera1;
            pTera1.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            pTera1.StartInfo.Arguments = "/f=" + setup.GetValue("config", "teraterm1configfile");
            pTera2.StartInfo = infoTera2;
            pTera2.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            pTera2.StartInfo.Arguments = "/f=" + setup.GetValue("config", "teraterm2configfile");
            pChrome.StartInfo = infoChrome;
            pChrome.StartInfo.WindowStyle = ProcessWindowStyle.Normal;

            AvvioProgrammi();
            SpostaFinestre();
            myTimer.Start();
            if (setup.GetValue("config","posizionamentoContinuo")!="Y")
                Application.Exit();
        }
    }
}